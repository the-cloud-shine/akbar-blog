---
title: Yousif Akbar
type: bio
---

I'm a Kuwaiti-Cuban American that's an AWS Certified DevOps Engineer by trade and a garden variety nerd. I like books, science, cooking and am always looking to get better at engaging with them. I currently work at PBS.

I was born and raised in Kuwait and moved to the US at 18 to study computer engineering at the University of Miami. Since then, I've moved up to the DMV area, and have been advancing on a DevOps journey.

I started another blog detailing some project work, then kind of got bored with it, so I decided to create this one just for personal stuff. If you're interested in tutorials on things related to AWS and building stuff in the cloud, check it out. If you find that there's anything that you'd like me to go over in greater detail or have ideas for any projects, drop me a line! I left a few ways for you to do so in the top right corner. The pencil icon is a link to the repo that built this blog on the page you are viewing. Feel free to fork this repo for your own blog, or make a Merge Request to fix mine!

I'm also really passionate about education. You will likely get a better picture of what I care about as I flesh out the Books section. If you've got a tech job that you're not thrilled about, or are stuck in a rut that you don't see a way out of, I'll try my best to help you. All I ask in exchange is that you teach me something. I have learned much more through informal mediums like this one than the formal schooling I've undergone. I hope that this can provide you a similar opportunity.
