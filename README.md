# Akbar Blog

I'm making this personal blog because I got bored with Pelican and wanted to try Hugo. I'm going to be writing about personal stuff. Not sure what personal stuff yet, but it's most likely going to start with book recommendations.

## Getting Started

If you'd like to fork this repo and play with it locally, do the following:

1) Install [Hugo][Hugo-Install].
2) Install the [Minimal Theme][Minimal-Theme]. You should only need to run `git submodule init && git submodule update`.
3) Run `hugo server` to start your local development server. It'll be accessible on [localhost][localhost].

## Deployments

Deployments on this project are orchestrated by [AWS Amplify][Amplify]. Because this project uses Hugo, Amplify treats it as a first class citizen on their platform. As a consequence, setting up deployments is as simple as connecting Amplify to your repo. The only modification that is required from a vanilla Hugo project implementation is the submodule activation that is accomplished on the [amplify.yml][amplify.yml-submodule].

In order to support environment specific configurations (as of this writing, the only thing using this is the git pencil), the `--environment` flag is set during `hugo` builds using the `${AWS_BRANCH}` environment variable exposed by Amplify. This allows for templates to populate data using the `.Hugo.Environment` variable to provide environment specific data.

[Hugo-Install]: https://gohugo.io/getting-started/installing/
[Minimal-Theme]: https://themes.gohugo.io/minimal/
[localhost]: http://localhost:1313/
[Amplify]: https://aws.amazon.com/amplify/
[amplify.yml-submodule]: ./amplify.yml#L6
